import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-loginsuc',
  templateUrl: './loginsuc.component.html',
  styleUrls: ['./loginsuc.component.css']
})
export class LoginsucComponent implements OnInit {


  constructor(public auth:AuthService) { }

  
  userId:string;
  useremail:string;

  ngOnInit() {
       
       this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          this.useremail = user.email;
         }
      )
  }

}