import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private authService:AuthService,
              private router:Router) { }

  email:string;
  password:string; 
  errorMessage:String;
  hasError:Boolean = false;

 
  onSubmit(){
    this.authService.Signup(this.email,this.password);
    //this.router.navigate(['/books']);
  }

  ngOnInit() {
    
    error =>{
      this.hasError = true;
      this.errorMessage = error.message;
      console.log("in the component " + error.message);

  }



  }
}

