import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';



import {MatExpansionModule} from '@angular/material/expansion'; 
import {MatCardModule} from '@angular/material/card';
import { FormsModule }   from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';



//firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';

import { WelcomeComponent } from './welcome/welcome.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ClassifyComponent } from './classify/classify.component';
import { ClassifiedComponent } from './classified/classified.component';
import { BooksComponent } from './books/books.component';
import { BookformComponent } from './bookform/bookform.component';
// import { EditbookComponent } from './editbook/editbook.component';
 import { LoginsucComponent } from './loginsuc/loginsuc.component';
import { PostsComponent } from './posts/posts.component';
import { ListComponent } from './list/list.component';
// import { ClassifiedComponent } from './classified/classified.component';

const appRoutes: Routes = [

  { path: 'welcome', component: WelcomeComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'loginsuc', component: LoginsucComponent},
  { path: 'posts', component: PostsComponent},
  { path: 'list', component: ListComponent},
  // { path: 'classified', component: ClassifiedComponent},
  // { path: 'classify', component: ClassifyComponent},
  // { path: 'bookform', component: BookformComponent},
  // { path: 'bookform/:id',  component: BookformComponent},
  // { path: 'books', component: BooksComponent },
//   { path: 'editbook/:id', component: EditbookComponent},

     { path: '',
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    WelcomeComponent,
    SignupComponent,
    LoginComponent,
    ClassifyComponent,
    ClassifiedComponent,
    BooksComponent,
    BookformComponent,
    LoginsucComponent,
    PostsComponent,
    ListComponent
  ],
 
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'test'),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
