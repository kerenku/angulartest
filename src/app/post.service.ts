import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './interfaces/post';
import { Comment } from './interfaces/comment';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostService {

  

  apiUrl = "https://jsonplaceholder.typicode.com/posts/";
  apicomment= "http://jsonplaceholder.typicode.com/comments" 

  
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postCollection:AngularFirestoreCollection;


  
  getPost(){    
    return this.http.get<Post[]>(this.apiUrl);
  }

  getComment(){    
    return this.http.get<Comment[]>(this.apicomment);
  }

 
  addPost(userId:string,title:string, body:string){
    const post = {title:title, body:body,like:0};
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  deletePost(id:string,userId:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  
  updatepostlike(userId:string, id:string,like:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
       {
         like:like
       }
     )
   }

  getPosts(userId:string):Observable<any[]>{
    this.postCollection=this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges().pipe(
      map(
        collection=>collection.map(
          document=>{
            const data= document.payload.doc.data();
            data.id= document.payload.doc.id;
            console.log(data);
            return data;
          }
        )
      )
    )
  }


  addlike(userId:string, id:string,like:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
       {
         like:like
       }
     )
   }

  
  
   constructor(private http:HttpClient,private db:AngularFirestore) { }
}
