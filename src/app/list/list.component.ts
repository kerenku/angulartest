import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  panelOpenState = false;
  userId:string;
  posts$:Observable<any>;
  like:number; 

  constructor(private PostService:PostService, public authService:AuthService) { }

  addlike(id:string,likes:number){
    this.like = likes + 1 ; 
    this.PostService.updatepostlike(this.userId,id,this.like)
  }

 
  deletePost(id:string){
    this.PostService.deletePost(id,this.userId);
  }
  ngOnInit() {
    
    this.authService.user.subscribe(
      user=>{
        this.userId=user.uid;
        this.posts$ = this.PostService.getPosts(this.userId);
      }
    )
        
       
    
      }
    
    }
    