import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  constructor(public afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;
  }

  getUser(){
    return this.user
  }


  Signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res => 
          {
            console.log('Succesful sign up',res);
            this.router.navigate(['/loginsuc']);
          }
        )
        
     .catch(function(error) {
      // Handle Errors here.
      // var errorCode = error.code;
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorMessage);
      console.log(error);
      console.log(errorCode);
      this.router.navigate(['/login']);
    });
  }

  Logout(){
    this.afAuth.auth.signOut();  
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/loginsuc']);
            }     
        )
  
        .catch(function(error) {
          // Handle Errors here.
          // var errorCode = error.code;
          var errorCode = error.code;
          var errorMessage = error.message;
          alert(errorMessage);
          console.log(error);
          console.log(errorCode);
          this.router.navigate(['/login']);
        });
      }
    

}
