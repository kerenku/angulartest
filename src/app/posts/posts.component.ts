import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PostService } from '../post.service';
import { Post } from '../interfaces/post';
import { Router } from '@angular/router';
import { Comment } from '../interfaces/comment';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {


  Posts$: Post[];
  comments$: Comment[];
  userId:string;
  // body:string;
  // title:string;
  message:string;
  postId:number; 

  constructor(private postservice: PostService,private auth:AuthService) { }

 
  // onSubmit(){
  //   this.body=this.postservice.totle;
  //   console.log(this.userId);
  //   console.log(this.body);
  //   console.log(this.title);
  //   this.postservice.addPost(this.userId,this.body,this.title);
  //   this.message = "Saved for later viewing"
  // }
  
  addPost(title:string,body:string,id:number){
    this.postservice.addPost(this.userId,title,body)
    this.postId = id;
    this.message = "Saved for later viewing"
  }

  ngOnInit() {
    this.postservice.getPost()
    .subscribe(data =>this.Posts$ = data );
    this.postservice.getComment()
    .subscribe(data =>this.comments$ = data );

    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId)
       }
    )
  }

}
