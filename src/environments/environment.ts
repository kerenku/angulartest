// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
  apiKey: "AIzaSyC5DN5bw2jJqF9zgAyMF0jx4RS9hm5Dwu4",
  authDomain: "test-967cc.firebaseapp.com",
  databaseURL: "https://test-967cc.firebaseio.com",
  projectId: "test-967cc",
  storageBucket: "test-967cc.appspot.com",
  messagingSenderId: "161476566697",

  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
